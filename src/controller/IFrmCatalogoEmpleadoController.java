/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import impl.DaoImplEmpleado;
import java.io.IOException;
import java.util.List;
import javax.swing.table.DefaultTableModel;
import pojo.Empleado;

/**
 *
 * @author Jadpa21
 */
public class IFrmCatalogoEmpleadoController {
    private DaoImplEmpleado daoEmpleado;
    private String header[] = {"Id","Cedula","INSS","Nombres",
        "Apellidos","Salario"};

    public IFrmCatalogoEmpleadoController() {
        daoEmpleado = new DaoImplEmpleado();
    }
    
    public Object[][] getData() throws IOException {
        return getData(daoEmpleado.getAll());
    }
    
    public Object[][] getData(List<Empleado> list) {
        Object[][] data = null;

        if (list.isEmpty()) {
            return data;
        }
        data = new Object[list.size()][header.length];
        int i = 0;

        for (Empleado e : list) {
            data[i] = getEmpleadoAsArray(e);
            i++;
        }

        return data;
    }
    
    public DefaultTableModel getTableModel() throws IOException{
        List<Empleado> empleados = daoEmpleado.getAll();
        Object[][] data = new Object[empleados.size()][header.length];
        int i = 0;
        for(Empleado e : empleados){
            data[i++] = getEmpleadoAsArray(e);
        }
        
        return new DefaultTableModel(data, header);
    }
    
    public Object[] getEmpleadoAsArray(Empleado e){
        Object[] singleData = new Object[header.length];
        singleData[0] = e.getId();
        singleData[1] = e.getCedula();
        singleData[2] = e.getInss();
        singleData[3] = e.getNombres();
        singleData[4] = e.getApellidos();
        singleData[5] = e.getSalario();
        
        return singleData;
    }
    
    public DefaultTableModel filterCase(String filter) throws IOException {
        Object[][] data = null;
        List<Empleado> listTmp = null;

        if (filter.equals("")) {
            data = getData();
            return new DefaultTableModel(data, header);
        }
        
        listTmp = daoEmpleado.findMany((e) -> e.getCedula().toLowerCase().startsWith(filter.toLowerCase()));
        if (listTmp == null) {
            listTmp = daoEmpleado.findMany((e) -> e.getInss().toLowerCase().startsWith(filter.toLowerCase()));
            if (listTmp == null) {
                listTmp = daoEmpleado.findMany((d) -> d.getApellidos().toLowerCase().startsWith(filter.toLowerCase()));
            }
        }

        if (listTmp != null) {
            data = getData(listTmp);
        } else {
            data = new Object[][]{null};
        }

        return new DefaultTableModel(data, header);
    }
    
}
