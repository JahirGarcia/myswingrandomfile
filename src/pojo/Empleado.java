/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pojo;

/**
 *
 * @author Jadpa21
 */
public class Empleado {
    private int id;//4
    private String cedula;//35
    private String inss;//21
    private String nombres;//43
    private String apellidos;//43
    private double salario;//8

    public Empleado() {
    }

    public Empleado(int id, String cedula, String inss, String nombres, String apellidos, double salario) {
        this.id = id;
        this.cedula = cedula;
        this.inss = inss;
        this.nombres = nombres;
        this.apellidos = apellidos;
        this.salario = salario;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public String getInss() {
        return inss;
    }

    public void setInss(String inss) {
        this.inss = inss;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public double getSalario() {
        return salario;
    }

    public void setSalario(double salario) {
        this.salario = salario;
    }
    
    
}
