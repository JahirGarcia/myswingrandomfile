/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package impl;

import dao.IDaoEmpleado;
import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import pojo.Empleado;

/**
 *
 * @author Jadpa21
 */
public class DaoImplEmpleado implements IDaoEmpleado{
    private RandomAccessFile hraf;
    private RandomAccessFile draf;
    private final int SIZE = 154 ;
    
    public DaoImplEmpleado() {
    }    
    
    private void open() throws IOException{
        File fileHeader = new File("empleadoHeader.dat");
        File fileData = new File("empleadoData.dat");
        
        if(!fileHeader.exists()){
            fileHeader.createNewFile();
            fileData.createNewFile();
            hraf = new RandomAccessFile(fileHeader, "rw");
            draf = new RandomAccessFile(fileData, "rw");
            hraf.seek(0);
            hraf.writeInt(0);
            hraf.writeInt(0);
        }else{
            hraf = new RandomAccessFile(fileHeader, "rw");
            draf = new RandomAccessFile(fileData, "rw");
        }        
    }
    
    public void close() throws IOException{
        if(hraf != null){
            hraf.close();
        }
        
        if(draf != null){
            draf.close();
        }
    }
    
    public Empleado findAny(Predicate<Empleado> p) throws IOException{
        Optional<Empleado> opt = getAll().stream().filter(p).findAny();        
        return opt.isPresent() ? opt.get() : null;
    }
    
    public List<Empleado> findMany(Predicate<Empleado> p) throws IOException{
        return getAll().stream().filter(p).collect(Collectors.toList());
    }
    
    @Override
    public Empleado findByCedula(String cedula) throws IOException {
        return this.getAll()
            .stream()
            .filter(e -> e.getCedula().equalsIgnoreCase(cedula))
            .findAny()
            .get();
    }

    @Override
    public Empleado findByInss(String inss) throws IOException {
        return this.getAll()
            .stream()
            .filter(e -> e.getInss().equals(inss))
            .findAny()
            .get();
    }

    @Override
    public List<Empleado> findByApellidos(String apellido) throws IOException {
        return this.getAll()
            .stream()
            .filter(e -> e.getApellidos().equalsIgnoreCase(apellido))
            .collect(Collectors.toList());
    }

    @Override
    public List<Empleado> findByRangoSalarial(double inf, double sup) throws IOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void save(Empleado t) throws IOException {
        open();
        hraf.seek(0);
        int n = hraf.readInt();
        int k = hraf.readInt();
        
        long pos = k * SIZE;
        
        draf.seek(pos);
        
        draf.writeInt(++k);
        draf.writeUTF(t.getCedula());
        draf.writeUTF(t.getInss());
        draf.writeUTF(t.getNombres());
        draf.writeUTF(t.getApellidos());
        draf.writeDouble(t.getSalario());
        
        hraf.seek(0);
        hraf.writeInt(++n);
        hraf.writeInt(k);
        
        long hpos = 8 + 4*(k-1);
        hraf.seek(hpos);
        hraf.writeInt(k);
        close();
    }

    @Override
    public int update(Empleado t) throws IOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean delete(Empleado t) throws IOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Empleado> getAll() throws IOException {
        open();
        List<Empleado> empleados = new ArrayList<>();
        hraf.seek(0);
        int n = hraf.readInt();
        
        for(int i = 0; i < n; i++){
            long hpos = 8 + 4*i;
            hraf.seek(hpos);
            int id = hraf.readInt();
            
            long dpos = (id - 1) * SIZE;
            draf.seek(dpos);
            
            Empleado e = new Empleado();
            e.setId(draf.readInt());
            e.setCedula(draf.readUTF());
            e.setInss(draf.readUTF());
            e.setNombres(draf.readUTF());
            e.setApellidos(draf.readUTF());
            e.setSalario(draf.readDouble());
            
            empleados.add(e);            
        }
        
        close();
        return empleados;
    }
    
}
